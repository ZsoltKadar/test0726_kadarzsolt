﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test0726_kadarzsolt
{
    class ForProfit : Company, IStock
    {
        public ForProfit(string name) : base(name)
        {
        }

        public ForProfit(string name, string address) : base(name, address) 
        {
        }

        public override void WriteDescription()
        {
            Console.WriteLine("Pénzt termelek a tulajdonosnak.");
        }
        
        public void BuyStock()
        {
            Console.WriteLine("For-profit cégként részvényt veszek.");
        }
    }
}
