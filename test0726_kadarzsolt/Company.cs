﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test0726_kadarzsolt
{
    public abstract class Company
    {

        protected string Name { get; }
        protected string Address { get; set; }


        public Company(string name)
        {
            Name = name;
        }

        public Company(string name, string address)
        {
            Name = name;
            Address = address;
        }

        public void WriteData()
        {
            Console.WriteLine("Company name: " + Name + "\nCompany address: " + Address);
        }

        public abstract void WriteDescription();
        

    }
}
