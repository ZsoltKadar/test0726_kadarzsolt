﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test0726_kadarzsolt
{
    class NonProfit : Company
    {
        public NonProfit(string name) : base(name)
        {
        }

        public NonProfit(string name, string address) : base(name, address)
        {
        }

        public override void WriteDescription()
        {
            Console.WriteLine("Segítek az embereken.");
        }
    }
}
