﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test0726_kadarzsolt
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Company> companies = new List<Company>
            {
                new NonProfit("Alapítvány1", "Budapest"),
                new ForProfit("Cég1", "Szeged"),
                new NonProfit("Alapítvány2", "Zalakaros"),
                new NonProfit("Alapítvány3", "Karancslapujtő"),
                new ForProfit("Cég2", "Nagykovácsi"),
                new ForProfit("Cég3", "Pécs"),
                new NonProfit("Alapítvány4", "Debrecen")
            };

            foreach (Company company in companies)
            {
                company.WriteData();
                company.WriteDescription();
                Console.WriteLine();
            }

            List<IStock> companies2 = new List<IStock>
            {
                new ForProfit("Cég4", "Mátészalka"),
                new Broker(),
            };

            foreach (IStock company in companies2)
            {
                company.BuyStock();
            }
            
            Console.ReadKey();
        }
    }
}
